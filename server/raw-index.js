const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());
const port = 5000;

app.use(express.static("public"));

//data
let posts = [{ id: "1", title: "MERN", body: "Im lerning mern" }];

const path = require("path");
app.use("/static", express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => {
  res.status(200).json({ message: "Success", posts: posts });
});

app.get("/:id", (req, res) => {
  const { id } = req.params;
  const post = posts.find((post) => post.id === id);

  if (!post) {
    return res.status(404).json({ message: "Failure" });
  }
  res.status(200).json({ message: "Success", posts: posts });
});

app.post("/", (req, res) => {
  const { id, title, body } = req.body;
  posts.push({ id, title, body });

  res.status(200).json({ message: "Success", post: { id, title, body } });
});
app.put("/:id", (req, res) => {
  const { id } = req.params;
  const { title, body } = req.body;

  const index = posts.findIndex((post) => post.id === id);
  if (index < 0) {
    return res.status(404).json({ message: "Failure" });
  }
  posts[index] = { id, title, body };
  res.status(200).send({ message: "Success", post: { id, title, body } });
});

app.patch("/:id", (req, res) => {
  const { id } = req.params;
  const { title } = req.body;

  const index = posts.findIndex((post) => post.id === id);
  console.log("printing index", index);

  if (index < 0) {
    return res.status(404).json({ message: "Failure" });
  }
  posts[index] = { ...posts[index], title };
  res.status(200).send({ message: "Success", posts: posts[index] });
});
app.delete("/:id", (req, res) => {
  const { id } = req.params;

  posts = posts.filter((post) => post.id !== id);
  res.status(200).json({ message: "Success" });
});

app.listen(port, () => {
  console.log(`server is running at ${port}`);
});

//---------------------------------------------------------------------------------------------------------//

///DB CONNECTION WITH MONGOOSE CLOUD
const express = require("express");
const mongoose = require("mongoose");

const { Schema, model } = mongoose;

const app = express();
const port = 5000;

app.use(express.static("public"));

const path = require("path");
app.use("/static", express.static(path.join(__dirname, "public")));
app.use(express.json());

mongoose
  .connect(
    "mongodb+srv://development:development@cluster0.8eizc.mongodb.net/e-commerce?retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("Connected to mongodb");
  })
  .catch((err) => {
    console.log(`Unable to connect mongodb... ${err}`);
  });

const postSchema = new Schema({
  title: String,
  body: String,
});

const Post = model("Post", postSchema);

app.get("/", async (req, res) => {
  const posts = await Post.find();
  res.status(200).json({ message: "success", posts });
});

app.get("/:id", async (req, res) => {
  const { id } = req.params;
  const post = await Post.findById(id);
  res.status(200).json({ message: "success", post });
});

app.post("/", (req, res) => {
  const { title, body } = req.body;
  const post = new Post({ title, body });
  post.save();
  res.status(200).json({ message: "success", post });
});
app.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { title, body } = req.body;
  const post = await Post.findByIdAndUpdate(id, { title, body }, { new: true });
  res.status(200).json({ message: "success", post });
});

app.patch("/:id", async (req, res) => {
  const { id } = req.params;
  const { title } = req.body;
  const post = await Post.findByIdAndUpdate(id, { title }, { new: true });
  res.status(200).json({ message: "success", post });
});

app.delete("/:id", async (req, res) => {
  const { id } = req.params;
  const post = await Post.findByIdAndDelete(id);
  res.status(200).json({ message: "data deleted success", post });
});

app.use((req, res, next) => {
  res.status(400).json({ message: "No Page Found!!" });
});

app.listen(port, () => {
  console.log(`server is running at ${port}`);
});
