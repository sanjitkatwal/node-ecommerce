const express = require("express");
const routes = require("./src/routes");

const app = express();

app.use(express.json());

app.use("/api/v1", routes);

app.use((req, res, next) => {
  res.status(404).json({ success: false, error: "No routes found!" });
});

app.use((err, req, res, next) => {
  res.status(err.status || 500).json({ success: false, error: err.message });
});

module.exports = app;
