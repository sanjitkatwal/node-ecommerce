require("dotenv").config();

const app = require("./index");
const databaseHelper = require("./lib/db.helper");

const { port, env } = require("./lib/config/env.variables");

databaseHelper();

console.log(env);

app.listen(port, () => {
  console.log(`Server is started at port number ${port} in ${env} enviroment`);
});
