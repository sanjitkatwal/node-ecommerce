const userService = require("../../src/user/user.service");
const errorHelper = require("../helpers/error.helpers");
const tokenHelper = require("../helpers/token.helper");
const { secretKey } = require("../config/env.variables");

const checkIfTokenExist = (token) => {
  //const token = data.split(" ")[1]; //Bearer token ['Bearer','token'];
  if (!token) {
    errorHelper("Token not found.", 404);
  }
  return token.split(" ")[1];
};

exports.isAuthenticated = async (req, res, next) => {
  try {
    const { authorization } = req.headers;

    const token = checkIfTokenExist(authorization);

    const decoded = await tokenHelper.verify(token, secretKey);

    const { _id } = decoded;

    await userService.getUserById(_id);

    req.user = { ...decoded };

    next();
  } catch (error) {
    console.log("middleware", error);
    next(error);
  }
};
//test