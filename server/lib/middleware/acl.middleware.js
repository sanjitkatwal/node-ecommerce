const errorHelper = require("../helpers/error.helpers")

/*
*
* @param {Array} roles
* @param {String} role
* @returns
* */
const checkIfRoleExist = (roles, role) =>{
    console.log('check if user exists >>', roles.includes(role) )
    return roles.includes(role);
}

exports.checkAccess = (roles=[]) =>{
    return (req, res, next) =>{
        try {
            const {role} = req.user;
            console.log('role is >>', role)
            const hasAccess = checkIfRoleExist(roles, role);
            if(!hasAccess){
                errorHelper("User doesn't have necessary privilege.", 403)
            }
            next();
        }catch (error){
            next(error)
        }
    }
}