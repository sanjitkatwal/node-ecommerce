const { Model } = require("mongoose");

exports.save = (Model, data) => {
  return new Model(data).save();
};

exports.find = (Model) => {
  return Model.find();
};

exports.findById = (Model, id) => {
  return Model.findById(id);
};

exports.findByIdAndPopulate = (Model, id, populateOptions) => {
  const { path = ``, select = `-_id` } = populateOptions;
  return Model.findById(id).populate({ path, select });
};

exports.findOne = (Model, query) => {
  return Model.findOne(query);
};

exports.findByIdAndUpdate = (Model, id, data) => {
  return Model.findByIdAndUpdate(id, data, { new: true });
};

exports.findByOneAndUpdate = (Model, query, data) => {
  return Model.findByOneAndUpdate(query, data, { new: true });
};

exports.findByIdAndDelete = (Model, id) => {
  return Model.findByIdAndDelete(id);
};

exports.findByOneAndDelete = (Model, query) => {
  return Model.findByOneAndDelete(query);
};
