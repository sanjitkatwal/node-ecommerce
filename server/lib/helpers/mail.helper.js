const nodemailer = require("nodemailer");
const errorHelper = require("./error.helpers");
const envVariables = require("../config/env.variables");

const transporter = nodemailer.createTransport({
  host: envVariables.smtpHost,
  port: envVariables.smtpPort,
  secure: false,
  auth: {
    user: envVariables.smtpUser,
    pass: envVariables.smtpPass,
  },
});

const mailSender = (email, subject, message) => {
  const mailOptions = {
    from: ``,
    to: email,
    subject,
    html: message,
  };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) errorHelper(error);
    console.log(`Email send ${info}`);
  });
};

module.exports = mailSender;
