const jwt = require("jsonwebtoken");
const uuid = require("uuid");

exports.uniqueCode = () => uuid.v4();

exports.sign = (payload, secretKey, signInOptions) => {
  return new Promise((resolve, reject) => {
    jwt.sign(payload, secretKey, signInOptions, (error, token) => {
      if (error) reject(error);
      resolve(token);
    });
  });
};

exports.verify = (token, secretKey) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secretKey, (error, token) => {
      if (error){
        error.status = 401;
        reject(error);
      };
      resolve(token);
    });
  });
};
