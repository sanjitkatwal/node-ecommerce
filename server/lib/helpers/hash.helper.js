const bcrypt = require("bcrypt");

exports.hash = (input) => {
  return new Promise((resolve, reject) => {
    bcrypt
      .hash(input, 10)
      .then((hash) => resolve(hash))
      .catch((error) => reject.error);
  });
};

exports.compare = (input, hash) => {
  return new Promise((resolve, reject) => {
    bcrypt
      .compare(input, hash)
      .then((match) => resolve(match))
      .catch((error) => reject.error);
  });
};
