const errorHelper = (message, status = 500) => {
  let error = new Error(message);
  error.status = status;
  error.success = false;
  throw error;
};

module.exports = errorHelper;
