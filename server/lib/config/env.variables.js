const variables = {
  port: process.env.PORT,
  env: process.env.NODE_ENV,
  mongodbUriDev: process.env.MONGODB_URI_DEV,
  mongodbUriProd: process.env.MONGODB_URI_PROD,

  // this is for token based authentication
  secretKey: process.env.JWT_SECRET_KEY,
  expiresIn: process.env.JWT_EXPIRES_IN,
  algorithm: process.env.JWT_ALGORITHM,

  // this is for email setup
  smtpHost: process.env.SMTP_HOST,
  smtpPort: process.env.SMTP_PORT,
  smtpUser: process.env.SMTP_AUTH_USER,
  smtpPass: process.env.SMTP_AUTH_PASS,
};

module.exports = variables;
