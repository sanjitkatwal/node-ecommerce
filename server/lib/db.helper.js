const mongoose = require("mongoose");

const {
  port,
  env,
  mongodbUriDev,
  mongodbUriProd,
} = require("./config/env.variables");

const databaseHelper = () => {
  let dbUri = "";

  switch (env) {
    case "development":
      dbUri = mongodbUriDev;
      break;
    case "production":
      dbUri = mongodbUriProd;
      break;
    default:
      dbUri = mongodbUriDev;
      break;
  }

  mongoose.connect(dbUri);

  mongoose.connection.on("connected", () => {
    console.log("Connected to MongoDB");
  });

  mongoose.connection.on("error", (error) => {
    console.log(`Unable to connect to MongoDB ${error}`);
  });

  mongoose.connection.on("disconnected", (error) => {
    console.log(`Database disconnected`);
  });
};

module.exports = databaseHelper;
