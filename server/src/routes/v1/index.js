const express = require("express");
const router = express.Router();

const userRoutes = require("../../user/user.route");
const authRoutes = require("../../auth/auth.route");
const categoryRoutes = require("../../category/category.route");

router.use("/users", userRoutes);
router.use("/auth", authRoutes);
router.use("/categories", categoryRoutes);

module.exports = router;
