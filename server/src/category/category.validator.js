const Joi = require("joi");

exports.createCategory = (data) => {
  const schema = Joi.object({
    name: Joi.string().required().trim().lowercase().label("Name"),
  });
  return schema.validate(data);
};
