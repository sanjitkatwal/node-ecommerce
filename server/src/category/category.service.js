const Category = require("./catagory.model");
const repository = require("../../lib/repository");
const errorHelper = require("../../lib/helpers/error.helpers");

exports.listCategories = async (req, res) => {
  return await repository.find(Category);
};

exports.getCategoryById = async (id) => {
  const category = await repository.findByIdAndPopulate(Category, id, {
    path: "createdBy",
    select: "firstName lastName email -_id",
    // select: { firstName: 1, lastName: 1, _id: 0 },
  });

  if (!category) {
    errorHelper("Category not found.", 404);
  }
  return category;
};

exports.checkIfCategoryExist = async (name) => {
  const category = await repository.findOne(Category, { name });
  if (category) {
    errorHelper("Category already exists!", 409);
  }

  return category;
};

exports.createCategory = async (value) => {
  return await repository.save(Category, value);
};

exports.updateCategory = async (id, value) => {
  const category = await repository.findByIdAndUpdate(Category, id, {
    ...value,
    updatedAt: Date.now(),
  });
  if (!category) {
    errorHelper("Category not found", 404);
  }
  return category;
};

exports.deleteCategory = async (id) => {
  const category = await repository.findByIdAndDelete(Category, id);
  if (!category) {
    errorHelper("Category not found", 404);
  }
  return category;
};
