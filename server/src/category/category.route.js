const authMiddleware = require("../../lib/middleware/auth.middleware");
const aclMiddleware = require("../../lib/middleware/acl.middleware");
const categoryController = require("./category.controller");
const express = require("express");

const router = express.Router();

// router.use(authMiddleware.isAuthenticated)

router.route("/").get(categoryController.listCategories);
router.route("/:id").get(categoryController.getCategoryById);

router.use(authMiddleware.isAuthenticated),
    aclMiddleware.checkAccess(["admin"])

router
    .route("/").post(categoryController.createCategory);

router
    .route("/:id")
    .put(categoryController.updateCategory)
    .delete(categoryController.deleteCategory);

module.exports = router;


/*const express = require("express");
const categoryController = require("./category.controller");
const authMiddleware = require("../../lib/middleware/auth.middleware");

const router = express.Router();

// router.use(authMiddleware.isAuthenticated)

router
  .route("/")
  .get(categoryController.listCategories)
  .post(authMiddleware.isAuthenticated, categoryController.createCategory);
router
  .route("/:id")
  .get(categoryController.getCategoryById)
  .put(authMiddleware.isAuthenticated, categoryController.updateCategory)
  .delete(authMiddleware.isAuthenticated, categoryController.deleteCategory);

module.exports = router;*/
