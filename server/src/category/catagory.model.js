const { Schema, model } = require("mongoose");

const categorySchema = new Schema({
  name: { type: String, required: "Name is required" },
  active: { type: Boolean, default: true },
  createdAt: { type: Date, default: Date.now },
  createdBy: { type: Schema.Types.ObjectId, ref: "User" },
  updatedAt: { type: Date },
  updatedBy: { type: Schema.Types.ObjectId, ref: "User" },
});

module.exports = model("Category", categorySchema);
