const errorHelper = require("../../lib/helpers/error.helpers");
const Catagory = require("./catagory.model");
const categoryService = require("./category.service");
const categoryValidator = require("./category.validator");

exports.listCategories = async (req, res, next) => {
  try {
    const categories = await categoryService.listCategories();

    res.status(200).json({
      success: true,
      message: "Categories listed successfully",
      categories,
    });
  } catch (error) {
    next(error);
  }
};

exports.getCategoryById = async (req, res, next) => {
  try {
    const { id } = req.params;
    const category = await categoryService.getCategoryById(id);
    if (!category) {
      errorHelper("Category not found.", 404);
    }
    res.status(200).json({
      success: true,
      message: "Category fetched successfully.",
      category,
    });
  } catch (error) {
    next(error);
  }
};

exports.createCategory = async (req, res, next) => {
  try {
    const { _id } = req.user;
    const { value, error } = categoryValidator.createCategory(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }

    const { name } = value;
    let category = await categoryService.checkIfCategoryExist(name);

    category = await categoryService.createCategory({
      name,
      createdBy: _id,
    });

    res.status(201).json({
      success: true,
      message: "Category created successfully",
      category,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateCategory = async (req, res, next) => {
  try {
    const { _id } = req.user;
    const { id } = req.params;
    const { value, error } = categoryValidator.createCategory(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }
    const { name } = value;
    let category = await categoryService.checkIfCategoryExist(name);

    category = await categoryService.updateCategory(id, {
      name,
      updatedBy: _id,
    });

    res.status(200).json({
      success: true,
      message: "Category updated successfully",
      category,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteCategory = async (req, res, next) => {
  try {
    const { id } = req.params;

    const category = await categoryService.deleteCategory(id);

    res.status(200).json({
      success: true,
      message: "Category deleted successfully",
      category,
    });
  } catch (error) {
    next(error);
  }
};
