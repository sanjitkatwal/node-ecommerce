const Joi = require("joi");

exports.login = (data) => {
  const schema = Joi.object({
    email: Joi.string().email().required().trim().lowercase().label("Email"),
    password: Joi.string().required().min(5).label("Password"),
  });
  return schema.validate(data);
};

exports.verifyUser = (data) => {
  const schema = Joi.object({
    verificationCode: Joi.string()
      .uuid()
      .required()
      .trim()
      .lowercase()
      .label("Verification code"),
    email: Joi.string().email().required().trim().lowercase().label("Email"),
  });
  return schema.validate(data);
};

exports.forgotPassword = (data) => {
  const schema = Joi.object({
    email: Joi.string().email().required().trim().lowercase().label("Email"),
  });
  return schema.validate(data);
};

exports.resetPassword = (data) => {
  const schema = Joi.object({
    verificationCode: Joi.string()
      .uuid()
      .required()
      .trim()
      .lowercase()
      .label("Verification code"),
    email: Joi.string().email().required().trim().lowercase().label("Email"),
    password: Joi.string().required().min(5).label("Password"),
  });
  return schema.validate(data);
};
