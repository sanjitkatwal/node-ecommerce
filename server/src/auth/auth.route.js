const authController = require("./auth.controller");

const express = require("express");
const app = require("../..");
const router = express.Router();

router.route("/login").post(authController.login);
router.route("/verify").post(authController.verifyUser);
router.route("/forgot-password").post(authController.forgotPassword);
router.route("/reset-password").post(authController.resetPassword);

module.exports = router;
