const errorHelper = require("../../lib/helpers/error.helpers");
const envVariables = require("../../lib/config/env.variables");
const tokenHelper = require("../../lib/helpers/token.helper");
const hashHelper = require("../../lib/helpers/hash.helper");
const mailHelper = require("../../lib/helpers/mail.helper");
const userService = require("../user/user.service");
const authValidator = require("./auth.validator");
const authService = require("./auth.service");

const { secretKey, expiresIn, algorithm } = envVariables;
const now = new Date();

exports.login = async (req, res, next) => {
  try {
    const { value, error } = authValidator.login(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }
    const { email, password } = value;
    const user = await authService.findUserByEmail(email);
    if (!user.active) {
      errorHelper("User not verified. Please verify user", 400);
    }
    const match = await hashHelper.compare(password, user.password);
    if (!match) {
      errorHelper("Invalid email or password", 400);
    }
    const payload = { _id: user._id, role: user.role };
    const token = await tokenHelper.sign(payload, secretKey, {
      expiresIn,
      algorithm,
    });
    res
      .status(200)
      .json({ success: true, message: "User Logged in successfully", token });
  } catch (error) {
    next(error);
  }
};

exports.verifyUser = async (req, res, next) => {
  try {
    const { value, error } = authValidator.verifyUser(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }
    const { email, verificationCode } = value;

    let user = await authService.findUserByEmail(email);
    const { _id, expiresIn, verificationCode: hashedVerificationCode } = user;
    const isExpired = now > new Date(expiresIn);
    if (isExpired) {
      errorHelper("Verification code expired. Please, request again!", 400);
    }
    const match = await hashHelper.compare(
      verificationCode,
      hashedVerificationCode
    );
    if (!match) {
      errorHelper("Invalid verification code", 400);
    }
    user = await userService.updateUser(_id, {
      active: true,
      updatedAt: Date.now(),
    });
    res
      .status(200)
      .json({ success: true, message: "User Verified successfully", user });
  } catch (error) {
    next(error);
  }
};

exports.forgotPassword = async (req, res, next) => {
  try {
    const { value, error } = authValidator.forgotPassword(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }
    const { email } = value;
    let user = await userService.findUserByEmail(email);
    const { _id, firstName, lastName } = user;

    const verificationCode = tokenHelper.uniqueCode();
    const hashVerificationCode = await hashHelper.hash(verificationCode);

    user = await userService.updateUser(_id, {
      verificationCode: hashVerificationCode,
      expiresIn: now.setMinutes(now.getMinutes() + 30),
    });

    mailHelper(
      email,
      "Please reset your password",
      `Hi! ${firstName} ${lastName} you have requiested to reset your password.
      This code is only valid for 30 minutes ${verificationCode} ${email}`
    );
    res.status(201).json({
      success: true,
      message: "Reset password sent to email successsfully.",
      user,
    });
  } catch (error) {
    next(error);
  }
};

exports.resetPassword = async (req, res, next) => {
  try {
    const { value, error } = authValidator.resetPassword(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }
    const { email, verificationCode, password } = value;
    console.log(email);
    let user = await userService.findUserByEmail(email);
    console.log("jey");

    const { _id, expiresIn, verificationCode: hashedVerificationCode } = user;
    const isExpired = now > new Date(expiresIn);
    if (isExpired) {
      errorHelper("Verification code expired. Please, request again!", 400);
    }
    const match = await hashHelper.compare(
      verificationCode,
      hashedVerificationCode
    );
    if (!match) {
      errorHelper("Invalid verification code", 400);
    }
    const hashedPassword = await hashHelper.hash(password);
    user = await userService.updateUser(_id, { password: hashedPassword });

    res.status(201).json({
      success: true,
      message: "Password updated successfully.",
      user,
    });
  } catch (error) {
    next(error);
  }
};
