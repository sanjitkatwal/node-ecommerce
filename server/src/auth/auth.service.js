const errorHelper = require("../../lib/helpers/error.helpers");
const User = require("../user/user.model");
const repository = require("../../lib/repository");

exports.findUserByEmail = async (email) => {
  const user = await repository.findOne(User, { email });

  if (!user) {
    errorHelper("Invalid email or password", 400);
  }

  return user;
};
