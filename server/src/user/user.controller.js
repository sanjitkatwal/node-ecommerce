const errorHelper = require("../../lib/helpers/error.helpers");
const hashHelper = require("../../lib/helpers/hash.helper");
const mailHelper = require("../../lib/helpers/mail.helper");
const userService = require("./user.service");
const validator = require("./user.validator");
const tokenHelper = require("../../lib/helpers/token.helper");

const now = new Date();

const listUsers = async (req, res, next) => {
  try {
    const users = await userService.listUsers();
    res.status(200).json({ success: true, users });
  } catch (error) {
    next(error);
  }
};

const getMe = async (req, res, next) => {
  try {
    const user = await userService.getUserById(req.params.id);
    res.status(200).json({ success: true, user });
  } catch (error) {
    next(error);
  }
};

const getUserById = async (req, res, next) => {
  try {
    const user = await userService.getUserById(req.params.id);
    res.status(200).json({ success: true, user });
  } catch (error) {
    next(error);
  }
};

const createUser = async (req, res, next) => {
  try {
    const { value, error } = validator.createUserValidator(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }

    const { firstName, lastName, email, password } = value;

    await userService.checkIfUserExist(email);

    const verificationCode = tokenHelper.uniqueCode();

    let hashedPassword = hashHelper.hash(password);
    let hashedVerificationCode = hashHelper.hash(verificationCode);
    [hashedPassword, hashedVerificationCode] = await Promise.all([
      hashedPassword,
      hashedVerificationCode,
    ]);
    const user = await userService.createUser({
      ...value,
      password: hashedPassword,
      verificationCode: hashedVerificationCode,
      expiresIn: now.setMinutes(now.getMinutes() + 30),
    });

    mailHelper(
      email,
      "Please verify your account",
      `Hi! ${firstName} ${lastName} you have created your account. Please verify your email.
      This code is only valid for 30 minutes ${verificationCode} ${email}`
    );
    res
      .status(201)
      .json({ success: true, message: "User Created Successsfully.", user });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

const updateUser = async (req, res, next) => {
  try {
    // TODO: UPDATE USER VALIDATOR
    // TODO: schema ma define gareko dekhi bahek data pathauda kasari handle garne?

    const { id } = req.params;
    const { value, error } = validator.updateUserValidator(req.body);
    if (error) {
      errorHelper(error.message, 400);
    }

    const user = await userService.updateUser(id, value);
    res
      .status(200)
      .json({ success: true, message: "User Updated Successfully", user });
  } catch (error) {
    next(error);
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = await userService.deleteUser(id);
    res
      .status(200)
      .json({ success: true, message: "User Deleted Successfully.", user });
  } catch (error) {
    next(errorHelper);
  }
};
module.exports = { listUsers, getUserById, createUser, updateUser, deleteUser, getMe };
