const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  firstName: { type: String, required: "First name is required" },
  lastName: { type: String, required: "Last name is required" },
  email: {
    type: String,
    unique: true,
    required: [true, "Email is required"],
    index: true,
  },
  password: { type: String, required: "Password is required" },
  role: {
    type: String,
    default: "customer",
    enum: ["admin", "seller", "customer"],
  },
  verificationCode: { type: String, required: "Verification code is required" },
  expiresIn: { type: Date, required: "Expired date is required" },
  active: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date },
  updatedBy: { type: Schema.Types.ObjectId, ref: "User" },
});

module.exports = model("User", userSchema);
