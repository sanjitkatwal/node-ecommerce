const authMiddleware = require("../../lib/middleware/auth.middleware")
const aclMiddleware = require("../../lib/middleware/acl.middleware")
const userController = require("./user.controller");
const express = require("express");
const router = express.Router();


router.route("/").post(userController.createUser);

router.use(
    authMiddleware.isAuthenticated
    //aclMiddleware.checkAccess(["admin"])
)

router.route("/me").get(userController.getMe);

router.use(aclMiddleware.checkAccess(["admin"]));

router.route("/").get(userController.listUsers);

router
  .route("/:id")
  .get(userController.getUserById)
  .put(userController.updateUser)
  .delete(userController.deleteUser);

module.exports = router;
