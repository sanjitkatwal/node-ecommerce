const errorHelper = require("../../lib/helpers/error.helpers");
const User = require("./user.model");
const repository = require("../../lib/repository");

exports.listUsers = async () => {
  return await repository.find(User);
};

exports.getUserById = async (id) => {
  const user = await repository.findById(User, id);
  if (!user) {
    errorHelper("User not Found", 404);
  }
  return user;
};

exports.findUserByEmail = async (email) => {
  const user = await repository.findOne(User, { email });
  if (!user) {
    errorHelper("User already exists", 409);
  }

  return user;
};

exports.checkIfUserExist = async (email) => {
  const user = await repository.findOne(User, { email });
  if (user) {
    errorHelper("User already exists!", 409);
  }

  return user;
};

exports.createUser = async (value) => {
  return await repository.save(User, value);
};

exports.updateUser = async (id, value) => {
  const user = await repository.findByIdAndUpdate(User, id, {
    ...value,
    updatedAt: Date.now(),
  });
  if (!user) {
    errorHelper("User not found", 404);
  }
  return user;
};

exports.deleteUser = async (id) => {
  const user = await repository.findByIdAndDelete(User, id);
  if (!user) {
    errorHelper("User not found", 404);
  }
  return user;
};
// module.exports = { listUsers, getUserById, createUser, updateUser, deleteUser };
