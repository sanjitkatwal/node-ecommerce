const Joi = require("joi");

const createUserValidator = (data) => {
  const schema = Joi.object({
    firstName: Joi.string().required().trim().label("First"),
    lastName: Joi.string().required().trim().label("Last"),
    email: Joi.string().email().required().trim().lowercase().label("Email"),
    role: Joi.string().required().trim().label("Role"),
    password: Joi.string().required().min(5).label("Password"),
  });
  return schema.validate(data);
};

const updateUserValidator = (data) => {
  const schema = Joi.object({
    firstName: Joi.string().trim().label("First"),
    lastName: Joi.string().trim().label("Last"),
    email: Joi.string().email().trim().lowercase().label("Email"),
    role: Joi.string().trim().label("Role"),
    password: Joi.string().min(5).label("Password"),
  });
  return schema.validate(data);
};

module.exports = { createUserValidator, updateUserValidator };
