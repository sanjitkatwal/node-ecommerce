const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.status(200).json({ message: "Hello from the server side!!!" });
});

app.post("/", (req, res) => {
  res.send("post response");
});

app.put("/", (req, res) => {
  res.send("put response");
});

app.patch("/", (req, res) => {
  res.send("patch response");
});

app.delete("/", (req, res) => {
  res.send("delete response");
});

const port = 5000;
app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});
